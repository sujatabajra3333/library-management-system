import email
from site import USER_BASE
from urllib.parse import MAX_CACHE_SIZE
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
# Create your models here.

USER_TYPE = (
    ('Admin', 'Admin'),
    ('Student', 'Student'),
)    
class User(AbstractBaseUser):
    user_type = models.CharField(max_length=100, choices= USER_TYPE, default='Student')
    name = models.CharField(max_length=30,blank=True, help_text="Enter your full name")
    email= models.EmailField(unique=True)
    created_date=models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD ='email'
    REQUIRED_FIELDS=['name', 'phone']

    def __str__(self):
        return self.name

    def book_borrowed_count(self):
        return self.bookborrow_set.filter(return_status=False).count()   
