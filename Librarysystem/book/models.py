from turtle import delay
from django.db import models
from student.models import User
# Create your models here.
class Book(models.Model):
    name = models.CharField(max_length=200)
    author= models.CharField(max_length=225)
    genre = models.CharField(max_length=255)
    total = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add=True)

class BookBorrower(models.Model):
    student = models.ForeignKey('User', on_delete=models.CASCADE)
    book = models.ForeignKey('Book', on_delete=models.CASCADE)
    issue_date = models.DateTimeField(null=True,blank=True)
    return_date = models.DateTimeField(null=True,blank=True)
    return_status= models.BooleanField(default=False)
    def __str__(self):
        return self.student.email+" borrowed "+self.book.name


class BookReviews(models.Model):
    review=models.CharField(max_length=100,default="none")
    book=models.ForeignKey('Book',on_delete=models.CASCADE)
    user=models.ForeignKey('User',on_delete=models.CASCADE)
    created_date = models.DateTimeField(null=True,blank=True)

    CHOICES = (
        ('0', '0'),
        ('.5', '.5'),
        ('1', '1'),
        ('1.5', '1.5'),
        ('2', '2'),
        ('2.5', '2.5'),
        ('3', '3'),
        ('3.5', '3.5'),
        ('4', '4'),
        ('4.5', '4.5'),
        ('5', '5'),
    )
 
    rating=models.CharField(max_length=3, choices=CHOICES, default='2')

    def __str__(self):
        return self.review


class BookExpiry(models.Model):
    book_borrower = models.OneToOneField(BookBorrower, on_delete=models.CASCADE)
    return_date = models.DateTimeField(null=True,blank=True)
    fine_per_day = models.PositiveIntegerField(default=5)
    delay_days = models.PositiveIntegerField(default=0)
    total_fine = models.PositiveIntegerField(default=0)

    def __str__(self)->str:
        return "Returned by"+self.book_borrower.student.email
